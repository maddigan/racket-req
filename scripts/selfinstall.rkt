#!/usr/bin/env racket


;; This file is part of racket-req - project dependency manager.
;; Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-req is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-req is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-req.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require upi/realpath
         upi/dirname
         (only-in threading ~>>)
         "../src/req-lib/req/req-file.rkt"
         "../src/req-lib/req/req.rkt")


(define (main)
  (let* ([project-root-path
          (~>> (find-system-path 'run-file)
               dirname
               (build-path _ "../")
               realpath)]
         [req-hash
          (req-file->hash (build-path project-root-path "req.json")
                          project-root-path)])
    (displayln "Installing Req by using itself...")
    (eprintf "Req project root is \"~a\".~%" project-root-path)
    (req req-hash 'all #false)))


(module+ main
  (main))
