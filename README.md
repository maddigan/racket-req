# Racket-Req

<p align="center">
    <a href="http://pkgs.racket-lang.org/package/req">
        <img src="./extras/badges/raco_pkg_install-req-aa00ff.svg">
    </a>
    <a href="https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.com/xgqt/racket-req">
        <img src="https://archive.softwareheritage.org/badge/origin/https://gitlab.com/xgqt/racket-req/">
    </a>
    <a href="https://gitlab.com/xgqt/racket-req/pipelines">
        <img src="https://gitlab.com/xgqt/racket-req/badges/master/pipeline.svg">
    </a>
</p>

Dependency manager for Racket projects.


## About

Req is a spiritual successor or Python's `requirement.txt` files,
but for Racket.

### Convenience

Req offers a convenient way to manage dependencies of large Racket packages.

Using the `raco req` command You can, for example:
- install only external dependencies,
- install all local packages at once,
- use a special catalogs repository for the project,
- save the extra packages You use for repository maintenance.

### File format

Contents of `req.json` file for multi-package projects
would have a form similar to the following:

```json
{
  "catalogs": ["https://catalogs_to_overwrite_default_catalogs.com"],
  "root": "./path/to/the/location/of/project/sub-packages",
  "local": [
    "project",
    "project-doc",
    "project-lib",
    "project-test"
  ],
  "dev": [
    "a-dependency-used-only-for-the-project-maintenance"
  ]
}
```

And the following is an example of single-package project:

```json
{
  "local": [
    [".", "project"]
  ]
}
```


## Installation

### Raco

Use raco to install Req from the official Racket Package Catalog.

```sh
raco pkg install --auto --skip-installed --user req
```

### Make

Use GNU Make to install Req from its project directory.

```sh
make install
```

### Req

Use Req itself to install it from its project directory.

```sh
raco pkg install --auto --skip-installed --user threading-lib upi-lib
racket ./scripts/selfinstall.rkt
```

Also, the `selfinstall` recipe from the Req's Makefile can be used.

``` sh
make selfinstall
```


## Documentation

Documentation can either be built locally with `make public`
or browsed online on either [GitLab pages](https://xgqt.gitlab.io/racket-req/)
or [Racket-Lang Docs](https://docs.racket-lang.org/req/).


## License

Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>

Licensed under the GNU GPL v2 License

SPDX-License-Identifier: GPL-2.0-or-later
