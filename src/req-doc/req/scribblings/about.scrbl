;; This file is part of racket-req - project dependency manager.
;; Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-req is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-req is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-req.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual


@title[#:tag "req-about"]{About}


Req is a command-line tool used to batch-install dependencies
of projects written in the Racket programming language.

Req is not a full replacement for @exec{raco pkg} on its own,
it still needs @filepath{info.rkt} files
to detect the project's local packages and gather dependent packages.

Req is in some sense a spiritual successor of
Python's @filepath{requirement.txt} files, but for Racket.
The biggest problem though with @filepath{requirement.txt}
was that dependency listing was duplicated in that file and @filepath{setup.py}'s
@exec{install_requires}.

Req offers a more convenient way of managing dependencies
of large Racket projects.

In Req You list sub-packages of Your projects and,
contrary to @filepath{requirement.txt}, the dependencies are extracted
automatically from @filepath{info.rkt} files of those sub-packages.
Also, because Req already requires a config file it is also used as
a mini-configuration for other project's areas,
so for example the catalogs that a given project should use can be
overwritten on the project level
and extra dependencies used for repository maintenance can be listed.

Using @seclink["req-cli"]{@exec{raco req}} it is possible to, for example:

@itemlist[
 @item{install only external dependencies,}
 @item{install all local packages at once,}
 @item{use a special catalogs repository for the project,}
 @item{record extra packages used for repository maintenance.}
 ]

First to use Req it is neded to
@seclink["req-tutorial"]{create a @filepath{req.json} file}
in the project's root.
