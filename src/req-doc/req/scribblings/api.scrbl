;; This file is part of racket-req - project dependency manager.
;; Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-req is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-req is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-req.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket
                     req/req
                     req/req-file
                     req/version)
          req/version)


@title[#:tag "req-api"]{Racket API}


The following documents how to use Req facilities in pure Racket code.


@section[#:tag "req-api-req"]{Running Req from Racket}

@defmodule[req/req]

@defproc[
 (req
  [req-hash hash?]
  [action symbol?]
  [action-argument (or/c #false string?) #false])
 void?
 ]{
 Executes the specified Req @racket[action].

 Specific action roles are already described in the Req
 @seclink["req-cli"]{command-line interface documentation}.

 @itemlist[
  @item{
   @racket[req-hash] is and object produced by
   the @racket[req-file->hash] function.
  }
  @item{
   @racket[action] is a @racket[symbol] name of the action,
   like the long @seclink["req-cli-action-flags"]{action flag} name.
  }
  @item{
   @racket[action-argument] is and optional @racket[string]
   that a specified @racket[action] accepts.
  }
  ]
 }

@defparam[
 req-reinstall-local? boolean boolean?
 #:value #false
 ]{
 Toggles whether any
 @seclink["req-supported-formats-keys-local"]{local packages}
 that are already installed when they are requested for installation
 will be reinstalled (first removed and then installed again).
 }


@section[#:tag "req-api-req-file"]{Parsing Req files}

@defmodule[req/req-file]

@defproc[
 (req-file->hash
  [req-file-path path-string?]
  [project-root-path path-string?])
 hash?
 ]{
 Produces a @racket[hash] object accepted by the @racket[req] function.
 }


@section[#:tag "req-api-version"]{Req Version}

@defmodule[req/version]

@defthing[VERSION string?]{
 Currently it is equal to @code{@format["~v" VERSION]}.
 }

@defthing[MAJOR exact-nonnegative-integer?]{
 Currently it is equal to @code{@format["~v" MAJOR]}.
 }

@defthing[MINOR exact-nonnegative-integer?]{
 Currently it is equal to @code{@format["~v" MINOR]}.
 }

@defthing[PATCH exact-nonnegative-integer?]{
 Currently it is equal to @code{@format["~v" PATCH]}.
 }
