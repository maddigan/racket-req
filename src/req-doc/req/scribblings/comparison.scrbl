;; This file is part of racket-req - project dependency manager.
;; Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-req is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-req is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-req.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual


@title[#:tag "req-comparison"]{Comparison}


I tend to compare Req to Python's @filepath{requirements.txt} files,
but more adequate comparison would be to the
@link["https://github.com/cask/cask/"]{
 @exec{cask} utility for GNU Emacs}.
This is because @filepath{requirements.txt} does not know anything about
project-local packages, because of this the dependencies tend to be duplicated
between @filepath{setup.py} and @filepath{requirement.txt}.

In Cask the user specifies the files that are used in the project and
dependencies listed in them are extracted.
In addition a "development" section can be defined containing dependencies
not necessary for the package runtime.

Example @filepath{Cask} file looks like the following
(taken from
@link["https://gitweb.gentoo.org/proj/company-ebuild.git/"]{
 company-ebuild}):

@nested[#:style 'code-inset]{@verbatim{
(source melpa)

(package-file "company-ebuild.el")

(files "company-ebuild-custom.el"
       "company-ebuild-keywords.el"
       "company-ebuild.el")

(development (depends-on "company")
             (depends-on "yasnippet"))
}}

Few similarities to Req files can be noted here:

@itemlist[
 @item{@racket{source}
  --- similar to
  @seclink["req-supported-formats-keys-catalogs"]{catalogs} attribute,}
 @item{@racket{package-file}/@racket{files}
  --- similar to
  @seclink["req-supported-formats-keys-local"]{local} attribute,}
 @item{@racket{development}
  --- similar to defining
  @seclink["req-supported-formats-keys"]{extra package sets}.}
 ]
