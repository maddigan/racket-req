;; This file is part of racket-req - project dependency manager.
;; Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-req is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-req is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-req.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual


@title[#:tag "req-tutorial"]{Tutorial}


@section[#:tag "req-tutorial-warning"]{Warning}

In addition to this tutorial, the document assumes that the user has read the
@link["https://blog.racket-lang.org/2017/10/tutorial-creating-a-package.html"
      ]{official tutorial on creating packages}.

This tutorial uses the
@seclink["req-supported-formats-json"]{JSON}
format.
I want to inform the user, that Req also supports the
@seclink["req-supported-formats-rktd"]{RKTD}
format that would probably please Symbolic-Expression lovers
(which most users of Racket probably are)
more than JSON.


@section[#:tag "req-tutorial-setup"]{Project setup}

Let's assume we are creating a package "mypkg".

We want to cut it into smaller packages so the users can install only
the library, only the docs and/or only the tests,
thus our project repository will contain:

@itemlist[
 @item{mypkg-lib - core library,}
 @item{mypkg-test - package containg tests,}
 @item{mypkg-doc - project documentation,}
 @item{mypkg - metapackage depending on the other 3 packages above.}
 ]

Let's put those packages into directory @filepath{src} in the project's root
and let's create adequate @filepath{info.rkt} and source files.
The structure of the project could look simialr to the following:

@nested[#:style 'code-inset]{@verbatim{
└── src/
    ├── mypkg/
    │   └── info.rkt
    ├── mypkg-doc/
    │   ├── info.rkt
    │   └── mypkg/
    │       └── scribblings/
    │           └── main.scrbl
    ├── mypkg-lib/
    │   ├── info.rkt
    │   └── mypkg/
    │       └── main.rkt
    └── mypkg-test/
        ├── info.rkt
        └── mypkg/
            └── tests/
                ├── integration/
                │   └── integration-test.rkt
                └── unit/
                    └── unit-test.rkt
}}

Normally if we would install a package from this project we would have to
invoke a rather long, forgettable @exec{raco} command
which would have to be typed for each local package,
for example:
@exec{raco pkg install --no-docs --skip-installed ./src/mypkg-lib}.

Req can bring a little bit of automation into the installation process.
With Req all of project's packages can be installed with one command.

First, create a @filepath{req.json} file at the project's root.

@nested[#:style 'code-inset]{@verbatim{
├── req.json
└── src/
    ├── mypkg/
    ├── mypkg-doc/
    ├── mypkg-lib/
    └── mypkg-test/
}}

Then, fill it with adequate data:

@itemlist[
 @item{project packages root is in src,
  so the @racket{root} key will hold a @racket{src} string,}
 @item{all of the packages begin with @racket{mypkg},
  so we can glob them using a @racket{mypkg*} string,
  thus the @racket{local} key
  (which lists projects's own packages)
  will hold @racket[(list "mypkg*")],}
 ]

Thus,
a basic Req file setup for our imaginary project will look like the following:

@nested[#:style 'code-inset]{@verbatim{
{
  "root": "src",
  "local": [
    "mypkg*"
  ]
}
}}


@section[#:tag "req-tutorial-command"]{The @exec{raco req} command}

After the project is set up we can invoke the @exec{raco req} command.

We can check the information of the project using
the @exec{raco req -s} comamnd.

@nested[#:style 'code-inset]{@verbatim{
catalogs:
local:
  - mypkg-test ✗
    /tmp/racket-mypkg/src/mypkg-test
  - mypkg-lib ✗
    /tmp/racket-mypkg/src/mypkg-lib
  - mypkg ✗
    /tmp/racket-mypkg/src/mypkg
  - mypkg-doc ✗
    /tmp/racket-mypkg/src/mypkg-doc
dependencies:
  - base ✓
  - racket-doc ✓
  - rackunit-lib ✓
  - scribble-lib ✓
extras:
}}

We can install all (not yet installed) dependencies
and local packages with @exec{raco req -A}.

If we would invoke the above command again we should see
Req informing us that all required packages are installed.

@nested[#:style 'code-inset]{@verbatim{
Package "racket-doc" already installed in "/usr/share/racket/pkgs/racket-doc".
Package "scribble-lib" already installed in "/usr/share/racket/pkgs/scribble-lib".
Package "rackunit-lib" already installed in "/usr/share/racket/pkgs/rackunit-lib".
Package "base" already installed in "/usr/share/racket/pkgs/base".
Package "mypkg-test" already installed in "/tmp/racket-mypkg/src/mypkg-test".
Package "mypkg-lib" already installed in "/tmp/racket-mypkg/src/mypkg-lib".
Package "req" already installed in "/tmp/racket-mypkg/src/mypkg".
Package "mypkg-doc" already installed in "/tmp/racket-mypkg/src/mypkg-doc".
}}


@section[#:tag "req-tutorial-further-reading"]{Further reading}

For more information about supported keys see
@seclink["req-supported-formats-keys"]{req-supported-formats-keys}.

For more information about supported formats see
@seclink["req-supported-formats"]{req-supported-formats}.

For more information about supported command-line options see
@seclink["req-cli"]{req-cli}.
