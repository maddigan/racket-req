;; This file is part of racket-req - project dependency manager.
;; Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-req is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-req is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-req.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual


@title[#:tag "req-external"]{External Resources}


@section{Posts}

@itemlist[
 @item{@link["https://racket.discourse.group/t/req-dependency-manager-for-racket-projects"]{
   Discourse: "Req: Dependency manager for Racket projects"}}]


@section{Upstream}

@itemlist[
 @item{@link["https://gitlab.com/xgqt/racket-req/"]{
   GitLab repository}}
 @item{@link["https://xgqt.gitlab.io/racket-req/"]{
   Documentation (GitLab pages)}}]


@section{Packages}

@itemlist[
 @item{@link["https://pkgs.racket-lang.org/package/req"]{
   req}}
 @item{@link["https://pkgs.racket-lang.org/package/req-doc"]{
   req-doc}}
 @item{@link["https://pkgs.racket-lang.org/package/req-lib"]{
   req-lib}}
 @item{@link["https://pkgs.racket-lang.org/package/req-test"]{
   req-test}}]


@section{Inspiration}

@itemlist[
 @item{@link["https://docs.microsoft.com/pl-pl/visualstudio/python/managing-required-packages-with-requirements-txt"]{
   MS VS documentation: requirements.txt}}
 @item{@link["https://cask.readthedocs.io/"]{
   Cask documentation}}
 @item{@link["https://github.com/cask/cask/"]{
   Cask upstream repository}}
 @item{@link["https://dune.readthedocs.io/"]{
   Dune documentation}}
 @item{@link["https://github.com/ocaml/dune/"]{
   Dune upstream repository}}]
