;; This file is part of racket-req - project dependency manager.
;; Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-req is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-req is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-req.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (only-in scribble/bnf nonterm))


@title[#:tag "req-cli"]{Command-line interface}


@section[#:tag "req-cli-synopsis"]{Synopsis}

Req invocation in any one of the following forms is accepted:

@itemlist[
 @item{@exec{raco req [action-flag] [option-flag] ...}}
 @item{@exec{raco req <info-flag>}}
 ]


@section[#:tag "req-cli-action-flags"]{Action flags}

Only up to one @exec{action-flag} can be given at a time.

The @exec{action-flag} is any one of the following:

@itemlist[
 @item{
  @Flag{s} or @DFlag{show}
  --- show Req setting of the current project.

  This is the default action that is executed if Req is run only as
  @exec{raco req}.

  The resulting output of this action will be similar to the following:

  @nested[#:style 'code-inset]{@verbatim{
    catalogs:
      - https://pkgs.racket-lang.org/
    local:
      - req-test ✓
        /home/user/source/public/gitlab.com/xgqt/racket-req/src/req-test
      - req-lib ✓
        /home/user/source/public/gitlab.com/xgqt/racket-req/src/req-lib
      - req ✓
        /home/user/source/public/gitlab.com/xgqt/racket-req/src/req
      - req-doc ✓
        /home/user/source/public/gitlab.com/xgqt/racket-req/src/req-doc
    dependencies:
      - base ✓
      - racket-doc ✓
      - rackunit-lib ✓
      - scribble-lib ✓
      - threading-lib ✓
      - upi-lib ✓
      - ziptie-git ✓
    extras:
      - dev:
        - ziptie-monorepo ✓
}}
 }
 @item{
  @Flag{d} or @DFlag{deps}
  --- install external dependencies of project's local packages.

  Dependencies are gathered from all info files of included local packages.
 }
 @item{
  @Flag{l} @nonterm{local-package-name}
  or @DFlag{local} @nonterm{local-package-name}
  --- install a @seclink["req-supported-formats-keys-local"]{local} package.
 }
 @item{
  @Flag{L} or @DFlag{locals}
  --- install all @seclink["req-supported-formats-keys-local"]{local} packages.
 }
 @item{
  @Flag{a} or @DFlag{all}
  --- install "deps" and "locals",

  in other words this flag acts as though running
  @DFlag{deps} followed by @DFlag{locals}.
 }
 @item{
  @Flag{e} @nonterm{extra-set-name} or @DFlag{extra} @nonterm{extra-set-name}
  --- install an
  @seclink["req-supported-formats-keys"]{extra set}
  of the name @nonterm{extra-set-name}.

  For example: if a set "dev" is defined as containing "pkgA" and "pkgB",
  then executing @exec{raco req -e dev} will install both of those packages.
 }
 @item{
  @Flag{E} or @DFlag{extras}
  --- install all extra sets.

  For example: if a set "A" is defined as containing "pkgA" and
  a set "B" is defined as containing "pkgB",
  then executing @exec{raco req -E}
  will install both packages from sets "A" and "B".
 }
 @item{
  @Flag{A} or @DFlag{everything}
  --- install "all" and "extras",
  in other words this flag acts as though running
  @DFlag{all} followed by @DFlag{extras}.
 }
 ]


@section[#:tag "req-cli-option-flags"]{Option flags}

Any different @exec{option-flag}s can be given at a time,
multiples of the same @exec{option-flag} are not accepted.

The @exec{option-flag} is any one of the following:

@itemlist[
 @item{
  @Flag{r} or @DFlag{reinstall}
  --- when the installation of
  @seclink["req-supported-formats-keys-local"]{local}
  packages is requested, if a local package is already installed,
  then it is first removed and installed afterwards.
 }
 @item{
  @Flag{p} @nonterm{dir-path} or @DFlag{project} @nonterm{dir-path}
  --- path to the project directory.
 }
 @item{
  @Flag{f} @nonterm{file-path} or @DFlag{file} @nonterm{file-path}
  --- path to the project's Req file in a supported format.

  The format parser to be used is chosen based on the file extension.
 }
 @item{
  @Flag{c} or @DFlag{no-colors}
  --- disable colors.
 }
 @item{
  @Flag{q} or @DFlag{quiet}
  --- be quiet (minimal/no console output).
 }
 @item{
  @Flag{v} or @DFlag{verbose}
  --- be verbose (detailed console output).
 }
 ]


@section[#:tag "req-cli-info-flags"]{Information flags}

If Req is invoked with a @exec{info-flag} only that and no other
@exec{action-flag}, @exec{option-flag} nor @exec{info-flag} can be given.

The @exec{info-flag} is any one of the following:

@itemlist[
 @item{
  @Flag{h} or @DFlag{help}
  --- show the help page.
 }
 @item{
  @Flag{V} or @DFlag{version}
  --- show the version of this program and exit.
 }
 ]
