;; This file is part of racket-req - project dependency manager.
;; Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-req is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-req is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-req.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket
                     req/version))


@title[#:tag "req-faq"]{Frequently Asked Questions}


@section[#:tag "req-faq-format"]{Why JSON?}

I wanted to mimic how JSON is used for Node and Python packaging.

I knew, however, that this idea will not be very welcome
in the so-called "Scheme community",
so Req also supports @seclink["req-supported-formats"]{other formats}.


@section[#:tag "req-faq-targets"]{What about setup, test, etc.?}

The scope of Req would be too wide if it supported mimicking all of the options
of @exec{raco}.

Compilation and setup can be substituted with using
the @seclink["req-cli-option-flags"]{@DFlag{reinstall}} flag.

Automating other actions like for example:
building documentation, binaries, archives or testing
can be done with any of:

@itemlist[
 @item{@link["https://www.gnu.org/software/make/"]{Makefiles}}
 @item{@link["https://github.com/Bogdanp/racket-chief/"]{Procfiles}}
 @item{@link["https://docs.racket-lang.org/zuo/index.html"]{Zuo}}
 @item{shell scripts}
 ]


@section[#:tag "req-faq-versions"]{What about requiring a specific version?}

Racket does not have extended facilities of
recording dependency versions and/or hashes similar to
Node, Julia or Python's Pipenv.

This can be worked around by:

@itemlist[
 @item{
  @seclink["req-assimilation"]{using git submodules},
 }
 @item{
  checking the package version or features and acting accordingly.

  Req's @racket[req/version] module defines the installed version,
  so to check Req's version the following code can be used:

  @racketblock[
  (require req/version)
  (unless (>= MAJOR 3)
    (error 'req-version
           "Req major version too low, wanted at least 3, given ~v"
           MAJOR))
  ]
 }
 ]


@section[#:tag "req-faq-strings"]{
  Where are @seclink["req-supported-formats-keys"]{strings} in the
  @seclink["req-supported-formats-rktd"]{RKTD examples}?}

The format of the file and the internal representation might differ,
best to look at the examples given in
the Req @seclink["req-supported-formats"]{formats} documentation.

The RKTD format for Req was thought to be as close to OCaml's Dune,
that's why everything is written as though it was a symbol,
string quotations are just not needed.
