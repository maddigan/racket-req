;; This file is part of racket-req - project dependency manager.
;; Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-req is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-req is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-req.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual


@title[#:tag "req-install"]{Install}


@section[#:tag "req-install-raco"]{Raco}

Use
@link["https://docs.racket-lang.org/raco/"]{raco}
to install Req from the official Racket
@link["https://pkgs.racket-lang.org/package/req"]{Package Catalog}.

@nested[#:style 'code-inset]{@verbatim{
raco pkg install --auto --skip-installed --user req
}}


@section[#:tag "req-install-make"]{Make}

Use
@link["https://www.gnu.org/software/make/"]{GNU Make}
to install Req from its project repository directory.

@nested[#:style 'code-inset]{@verbatim{
make install
}}


@section[#:tag "req-install-self"]{Req (self-installation)}

Use Req itself to install it from its project repository directory.

@nested[#:style 'code-inset]{@verbatim{
raco pkg install --auto --skip-installed --user threading-lib upi-lib
racket ./scripts/selfinstall.rkt
}}

Also, the @exec{selfinstall} recipe from the Req's Makefile can be used.

@nested[#:style 'code-inset]{@verbatim{
make selfinstall
}}
