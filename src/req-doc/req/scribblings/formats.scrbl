;; This file is part of racket-req - project dependency manager.
;; Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-req is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-req is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-req.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual


@title[#:tag "req-supported-formats"]{Supported formats}


The default Req file format is JSON but also
the Racket Data format is supported.

The format parser is picked based on the file extension,
so @filepath{.json} for @seclink["req-supported-formats-json"]{JSON}
and @filepath{.rktd} for @seclink["req-supported-formats-rktd"]{Racket Data}.
Because of that, giving Req a JSON file containing Racket data
will cause it to crash.

Also, Racket's JSON parser is very picky about given data,
this means, most importantly: no comments and no leftover commas.

By default if the flag @DFlag{file} is not given,
then Req will search the following paths, in order:

@itemlist[
 @item{@filepath{req.json}}
 @item{@filepath{.req.json}}
 @item{@filepath{.config/req.json}}
 @item{@filepath{req.rktd}}
 @item{@filepath{.req.rktd}}
 @item{@filepath{.config/req.rktd}}
 ]


@section[#:tag "req-supported-formats-keys"]{Special keys}

Any keys other than @racket{root}, @racket{catalogs} or @racket{local}
are treated as @racket{extra} set,
such set contains a list of packages that can be installed with
either @DFlag{extra} or @DFlag{extras} flag passed to @exec{raco req}
(see the Req command-line interface documentation for more details).

@racketgrammar*[
 [extra
  (list package-name ...)]
 [package-name
  string]
 ]


@subsection[#:tag "req-supported-formats-keys-root"]{root}

@racketgrammar*[
 [root
  string]
 ]

Specifies the location of root directory of project's sub-packages.

@subsection[#:tag "req-supported-formats-keys-catalogs"]{catalogs}

@racketgrammar*[
 [catalogs
  (list catalog-url ..)]
 [catalog-url
  string]
 ]

Specifies a list of catalogs to overwrite the default Racket catalogs.
If this list is not given or empty, then the default catalogs are used.

@subsection[#:tag "req-supported-formats-keys-local"]{local}

@racketgrammar*[
 [local
  (list package ...)]
 [package
  package-path
  (list package-path package-name)]
 [package-path
  string]
 [package-name
  string]
 ]

Specifies a list of local packages.

Each package can either be a path (inside the package root directory)
or a list containig two elements:

@itemlist[
 @item{path appended to package root directory path,}
 @item{name overwriting the default name of that package.}
 ]

If a string is used, then it can contain wildcards used
to glob directories inside the package root directory
(which can be overwritten by the @racket{root} key).


@section[#:tag "req-supported-formats-json"]{JSON}

Example of a single-package project using JSON as a configuration format:

@nested[#:style 'code-inset]{@verbatim{
{
  "local": [
    [".", "mylib"]
  ],
  "dev": [
    "ziptie-completion"
  ]
}
}}

Example of a multi-package project using JSON as a configuration format:

@nested[#:style 'code-inset]{@verbatim{
{
  "root": "src",
  "local": [
    "mylib*"
  ],
  "dev": [
    "ziptie-completion"
  ]
}
}}


@section[#:tag "req-supported-formats-rktd"]{Racket Data}

Example of a single-package project using RKTD as a configuration format:

@nested[#:style 'code-inset]{@verbatim{
((local ((. mylib)))
 (dev (ziptie-completion)))
}}

Example of a multi-package project using RKTD as a configuration format:

@nested[#:style 'code-inset]{@verbatim{
((root src)
 (local (mylib*))
 (dev (ziptie-completion)))
}}
