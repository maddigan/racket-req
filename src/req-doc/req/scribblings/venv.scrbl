;; This file is part of racket-req - project dependency manager.
;; Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-req is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-req is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-req.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual


@title[#:tag "req-venv"]{Req with virtual environments}


Req goes on very nicely with Racket virtual environments.

A popular package for managing Racket virtual environments is
@link["https://docs.racket-lang.org/raco-pkg-env/"]{raco-pkg-env}.

But also the following solution of exporting some environment variables can
be used:

@nested[#:style 'code-inset]{@verbatim{
rv=$(racket -e "(printf (version))")
export PS1="(venv) ${PS1}"
export PLTUSERHOME="$(pwd)/.cache/venv/${rv}/"
export PATH="$(pwd)/.cache/venv/${rv}/.local/share/racket/${rv}/bin/:${PATH}"
mkdir -p "${PLTUSERHOME}"
}}

The most important of those commands is the one setting @exec{PLTUSERHOME}.
