;; This file is part of racket-req - project dependency manager.
;; Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-req is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-req is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-req.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual


@title[#:tag "req-assimilation"]{Package assimialtion using submodules}


There is a option to treat any package that can be installed from a directory
as a @seclink["req-supported-formats-keys-local"]{local}
package - because of this there is a possibility of using
git submodules as local packages.

This means that Req does not have to record the git commit hashes
of the packages, like for example Pipenv or Julia package managers would do,
but git will keep that information instead.


@section[#:tag "req-assimilation-examples"]{Examples}


@subsection[#:tag "req-assimilation-example-asd"]{asd}

If a package "asd" comes from repository "https://gitlab.co/asd/asd/",
has package path in @filepath{src/asd-lib},
and is registered in local repository on path @filepath{src/asd},
then the inclusion in project "qwe" would be similar to the following:

@nested[#:style 'code-inset]{@verbatim{
{
  "local": [
    ["src/qwe", "qwe"],
    ["src/asd/src/asd-lib", "asd-lib"]
  ]
}
}}


@subsection[#:tag "req-assimilation-example-req"]{req}

If Req assimilated the package
@link["https://pkgs.racket-lang.org/package/ziptie-monorepo"]{ziptie-monorepo}
(into the path @filepath{src/racket-ziptie}),
then Req's configuration would look like the following:

@nested[#:style 'code-inset]{@verbatim{
{
  "root": "src",
  "local": [
    "req*",
    ["racket-ziptie/src/ziptie-monorepo", "ziptie-monorepo"]
  ]
}
}}

And the output of calling @exec{raco req -s} would be:

@nested[#:style 'code-inset]{@verbatim{
catalogs:
  - https://pkgs.racket-lang.org/
local:
  - ziptie-monorepo  ✓
    /home/user/source/public/gitlab.com/xgqt/racket-req/src/racket-ziptie/src/ziptie-monorepo
  - req-test ✓
    /home/user/source/public/gitlab.com/xgqt/racket-req/src/req-test
  - req-lib ✓
    /home/user/source/public/gitlab.com/xgqt/racket-req/src/req-lib
  - req ✓
    /home/user/source/public/gitlab.com/xgqt/racket-req/src/req
  - req-doc ✓
    /home/user/source/public/gitlab.com/xgqt/racket-req/src/req-doc
dependencies:
  - base ✓
  - racket-doc ✓
  - rackunit-lib ✓
  - scribble-lib ✓
  - threading-lib ✓
  - upi-lib ✓
  - ziptie-git ✓
extras:
}}
