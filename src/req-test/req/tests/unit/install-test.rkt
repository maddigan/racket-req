;; This file is part of racket-req - project dependency manager.
;; Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-req is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-req is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-req.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base


(module+ test
  (require rackunit
           colorize/optional
           req/install
           "../test-eequal.rkt")

  (colorize? #false)

  (test-eequal? "Do not setup a package with empty collections"
                (setup/custom "a" '())
                "! Setup for \"a\" is not necessary (empty collections).\n")

  (test-not-false "Package \"base\" is installed"
                  (installed? "base"))

  (test-false "Package \"\\\" is not installed"
              (installed? "\\"))

  (test-ematch? "Do not install already installed package \"base\""
                (install-pkgs (hash "base" #false))
                "already installed"))
